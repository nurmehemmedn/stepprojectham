let tabs = document.querySelectorAll('[data-target]');
let tabsContent = document.querySelectorAll('[data-tab-content]');

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        tabs.forEach(tab => {
            tab.classList.remove('active');
            tab.children[0].classList.remove('active');
        });
        const target = document.getElementById(tab.dataset.target);
        target.classList.add('active');
        target.children[0].classList.add('active');
        tabsContent.forEach(tabContent => {
            tabContent.classList.remove('activeService');
        });
        const targetContent = document.getElementById(tab.dataset.target.slice(1, tab.dataset.target.length));
        targetContent.classList.add('activeService');
    })
});

const _galleryTabs = [
    {
        title: 'All'
    },
    {
        title: 'Graphic Design'
    },
    {
        title: 'Web Design'
    },
    {
        title: 'Landing Pages'
    },
    {
        title: 'WordPress'
    }
];

elementUl = document.createElement('UL');
elementUl.classList.add('tabs');

_galleryTabs.forEach(tab => {
    const {title} = tab;
    elementLi = document.createElement('LI');
    elementLi.innerText = title;
    elementLi.classList.add('tabs-gallery-title');
    let arr = [...title];
    elementLi.id += '#';
    elementLi.dataset.tab = "";
    for (let item of arr) {
        if (item !== " ") {
            elementLi.id += item;
            elementLi.dataset.tab += item;
        }
    }

    elementUl.append(elementLi);
});


document.querySelector('.centered-gallery-content').append(elementUl);

const _Categories = [
    'web design',
    'graphic design',
    'wordpress',
    'landing pages'
];

const _Ids = [
    '#WebDesign',
    '#GraphicDesign',
    '#WordPress',
    '#LandingPages',
    '#All'
];

const _GalleryPhotos = [
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design1.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design2.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design3.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design4.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design5.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design6.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design7.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design2.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design3.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design4.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design5.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design6.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design7.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design2.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design3.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design4.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design5.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design6.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design7.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design2.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design3.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design4.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design5.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design6.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design7.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design2.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design3.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design4.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design5.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design6.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design7.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design2.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design3.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design4.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design5.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'web design',
        photoSrc: '../Images/webDesign/web-design6.jpg',
        category: _Categories[0],
        id: _Ids[0]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design1.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design2.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design3.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design4.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design5.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design6.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design8.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design9.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design10.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design12.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design1.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design2.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design3.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design4.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design5.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design6.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design8.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design9.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design10.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design12.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design1.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design2.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design3.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design4.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design5.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design6.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design8.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design9.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design10.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'graphic design',
        photoSrc: '../Images/graphicDesign/graphic-design11.jpg',
        category: _Categories[1],
        id: _Ids[1]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress1.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress2.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress3.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress4.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress5.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress6.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress7.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress8.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress9.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress10.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress1.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress2.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress1.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress2.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress3.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress4.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress5.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress6.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress7.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress8.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress9.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress10.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress1.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress2.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress1.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress2.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress3.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress4.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress5.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress6.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress7.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress8.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress9.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress10.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress1.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'wordpress',
        photoSrc: '../Images/wordPress/wordpress2.jpg',
        category: _Categories[2],
        id: _Ids[2]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page1.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page2.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page3.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page4.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page5.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page6.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page1.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page2.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page3.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page4.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page5.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page6.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page2.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page3.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page4.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page5.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page6.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page1.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page2.jpg',
        category: _Categories[3],
        id: _Ids[3]
    }
    ,
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page3.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page4.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page5.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page6.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page1.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page2.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page3.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page4.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page5.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page6.jpg',
        category: _Categories[3],
        id: _Ids[3]
    },
    {
        title: 'landing pages',
        photoSrc: '../Images/landingPages/landing-page7.jpg',
        category: _Categories[3],
        id: _Ids[3]
    }
];

let tabsGallery = document.querySelectorAll('[data-tab]');

let galleryId = "#All";

let itemsCount = 12;

document.getElementById('#All').classList.add('activeTab');
tabsGallery.forEach(item => {
    item.addEventListener('click', () => {
        galleryId = '#' + item.dataset.tab;
        tabsGallery.forEach(item => {
            item.classList.remove('activeTab');
        });
        contentMaker(galleryId);
    })
});


window.addEventListener('load', e => {
    for (let item = 0; item < itemsCount; item++) {

        if (galleryId === '#All') {
           hoverContentMakerForAll();
        }
    }
});

let clickCount=0;

document.querySelector('.gallery-button').addEventListener('click', () => {
    itemsCount += 12;
    clickCount++;
    if(clickCount === 2) {
        document.querySelector('.gallery-button').style.display = 'none';
    }
    tabsGallery.forEach(item => {
        item.classList.remove('activeTab');
    });
     contentMaker(galleryId);
});

function contentMaker(galleryId) {
    const target = document.getElementById(galleryId);
    target.classList.add('activeTab');
    let num;
    switch (galleryId) {
        case '#WebDesign' :
            num = 0;
            break;
        case '#GraphicDesign' :
            num = 36;
            break;
        case '#WordPress' :
            num = 72;
            break;
        case '#LandingPages' :
            num = 108;
            break;
        default :
            num = 0;
    }

    document.querySelector('.gallery-items').innerHTML = "";
    for (let item = num; item < num + itemsCount; item++) {
        const { title, photoSrc, category, id} = _GalleryPhotos[item];
        if (galleryId === '#All') {
            hoverContentMakerForAll();
        } else if (id === galleryId) {
            const itemContainer = document.createElement('DIV');
            const image = document.createElement('IMG');

            itemContainer.classList.add('item-style');
            image.classList.add('img-style');


            image.src = photoSrc;

            itemContainer.addEventListener('mouseenter', e => {
                const hoverDiv = document.createElement('DIV');
                const hoverDivContent = document.createElement('DIV');

                const firsticon = document.createElement('IMG');

                const firsticoninnner = document.createElement('IMG');
                const secondicon = document.createElement('IMG');
                const secondiconinner = document.createElement('IMG');
                const firsticondiv = document.createElement('DIV');
                firsticon.src = '../Images/Clipse-ellipse.png';
                secondicon.src = '../Images/search-ellipse.png';
                secondiconinner.src = '../Images/search-icon.png';
                firsticoninnner.src = '../Images/Forma-clipse.png';

                firsticon.classList.add('icon-style');
                secondicon.classList.add('icon-style');
                hoverDiv.classList.add('hoverdiv-style');
                hoverDivContent.classList.add('gallery-hover-content');
                firsticondiv.classList.add('firsticon-style');
                firsticoninnner.classList.add('firsticon-inner-style');
                secondiconinner.classList.add('secondicon-inner-style');


                firsticondiv.append(firsticon, secondicon, firsticoninnner, secondiconinner);


                itemContainer.append(firsticondiv);

                const imgTitle = document.createElement('H6');
                imgTitle.innerText = title;
                imgTitle.classList.add('gallery-title-style');
                const imgCategory = document.createElement('SPAN');
                imgCategory.innerText = category;
                imgCategory.classList.add('gallery-category-style');

                hoverDivContent.append(firsticondiv);
                hoverDivContent.append(imgTitle, imgCategory);
                hoverDiv.append(hoverDivContent);
                itemContainer.append(hoverDiv);
            });

            itemContainer.addEventListener('mouseleave', e => {
                itemContainer.querySelector('.hoverdiv-style').remove();
            });


            itemContainer.append(image);

            document.querySelector('.gallery-items').append(itemContainer);
        }
    }

}

function hoverContentMakerForAll() {
    let randomNumber = Math.floor(Math.random() * (_GalleryPhotos.length - 1));
    const itemContainer = document.createElement('DIV');

    const image = document.createElement('IMG');

    itemContainer.classList.add('item-style');
    image.classList.add('img-style');

    image.src = _GalleryPhotos[randomNumber].photoSrc;
    const {title, photoSrc, category, id} = _GalleryPhotos[randomNumber];
    itemContainer.addEventListener('mouseenter', e => {
        const hoverDiv = document.createElement('DIV');
        const hoverDivContent = document.createElement('DIV');

        const firsticon = document.createElement('IMG');

        const firsticoninnner = document.createElement('IMG');
        const secondicon = document.createElement('IMG');
        const secondiconinner = document.createElement('IMG');
        const firsticondiv = document.createElement('DIV');
        firsticon.src = '../Images/Clipse-ellipse.png';
        secondicon.src = '../Images/search-ellipse.png';
        secondiconinner.src = '../Images/search-icon.png';
        firsticoninnner.src = '../Images/Forma-clipse.png';

        firsticon.classList.add('icon-style');
        secondicon.classList.add('icon-style');
        hoverDiv.classList.add('hoverdiv-style');
        hoverDivContent.classList.add('gallery-hover-content');
        firsticondiv.classList.add('firsticon-style');
        firsticoninnner.classList.add('firsticon-inner-style');
        secondiconinner.classList.add('secondicon-inner-style');


        firsticondiv.append(firsticon, secondicon, firsticoninnner, secondiconinner);


        itemContainer.append(firsticondiv);

        const imgTitle = document.createElement('H6');
        imgTitle.innerText = title;
        imgTitle.classList.add('gallery-title-style');
        const imgCategory = document.createElement('SPAN');
        imgCategory.innerText = category;
        imgCategory.classList.add('gallery-category-style');

        hoverDivContent.append(firsticondiv);
        hoverDivContent.append(imgTitle, imgCategory);
        hoverDiv.append(hoverDivContent);
        itemContainer.append(hoverDiv);
    });
        itemContainer.addEventListener('mouseleave', e => {
            itemContainer.querySelector('.hoverdiv-style').remove();
        });


        itemContainer.append(image);

        document.querySelector('.gallery-items').append(itemContainer);
}













